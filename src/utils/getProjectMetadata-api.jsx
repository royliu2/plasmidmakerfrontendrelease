const getProjectMetaData = (cookies, setCookies, projects, setProjects) => {
  const authToken = `Token ${cookies.token}`;
  fetch(process.env.REACT_APP_DJANGO_API + 'obtain_user_information/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': authToken,
    },
  }).then((res) => {
    if (res.status !== 200) {
      res.json().then(() => {
        return false;
      });
    } else {
      res.json().then((data) => {
        const userMetaData = data.basic_info;
        setCookies('firstName', userMetaData.first_name);
        setCookies('lastName', userMetaData.last_name);
        setCookies('affiliation', userMetaData.affiliation);
        setCookies('technician', data.technician_status.is_technician);
        setCookies('orders', data.orders);
        setProjects(data.orders);
      });
    }
  });
};

export default getProjectMetaData;
