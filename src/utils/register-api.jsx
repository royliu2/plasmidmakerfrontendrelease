const registerApi = (username, email, password, confirmPassword, firstName,
    lastName, affiliation) => {
  const registerData = {
    'username': username,
    'email': email,
    'password': password,
    'confirm_password': confirmPassword,
    'first_name': firstName,
    'last_name': lastName,
    'affiliation': affiliation,
  };
  fetch(process.env.REACT_APP_DJANGO_API + 'api/register/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(registerData),
  }).then((res) => {
    if (res.status !== 200) {
      res.json().then(() => {
        return false;
      });
    } else {
      res.json().then((data) => {
        if ('errors' in data) {
          return false;
        }
        return true;
      });
    }
  });
};

export default registerApi;
