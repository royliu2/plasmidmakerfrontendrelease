import {useHistory} from 'react-router-dom';
import {useCookies} from 'react-cookie';
const verifyToken = () => {
  return true;
  const history = useHistory();
  const [cookies, setCookie] = useCookies(['token']);
  const postToken = {
    'verification_token': cookies,
  };
  fetch(process.env.REACT_APP_DJANGO_API + 'verify_token/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(postToken),
  }).then((res) => {
    if (res.status !== 200) {
      res.json().then(() => {
        setCookie('');
        history.push('/');
        return false;
      });
    } else {
      res.json().then((data) => {
        if (data.hasOwnProperty('token')) {
          if (data.token === cookies) {
            return;
          }
        } else {
          setCookie('');
          history.push('/');
        }
      });
    }
  });
};

export default verifyToken;
