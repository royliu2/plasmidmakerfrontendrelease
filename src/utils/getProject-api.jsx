const getProject = (projectId, setSequence, setFeatures, setPlasmidName,
    setVisualizerName, setVisualizerFeatures, setVisualizerSequence,
    cookie) => {
  const getOrder = {
    'order_id': projectId,
  };
  fetch(process.env.REACT_APP_DJANGO_API + 'get_order/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Token ${cookie.token}`,
    },
    body: JSON.stringify(getOrder),
  }).then((res) => {
    if (res.status !== 200) {
      res.json().then(() => {
        return false;
      });
    } else {
      res.json().then((data) => {
        setPlasmidName(data.name);
        setFeatures(data.features);
        setSequence(data.sequence);
        setVisualizerSequence(data.sequence);
        setVisualizerFeatures(data.features);
        setVisualizerName(data.name);
      });
    }
  });
};

export default getProject;
