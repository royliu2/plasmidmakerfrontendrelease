const searchApi = (query, setAlignments, setParts, setPlasmids) => {
  const queryData = {
    'search': query,
  };
  fetch(process.env.REACT_APP_DJANGO_API +
    'search_general_by_alignment_parts_and_plasmid_name/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(queryData),
  }).then((res) => {
    if (res.status !== 200) {
      res.json().then(() => {
        return false;
      });
    } else {
      res.json().then((data) => {
        setAlignments(data.alignments);
        setParts(data.parts);
        setPlasmids(data.plasmids);
        return;
      });
    }
  });
};

export default searchApi;
