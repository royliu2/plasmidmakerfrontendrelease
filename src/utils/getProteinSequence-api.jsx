const getProtein = (sequence, setTranslation) => {
  const getSequence = {
    'dna_sequence': sequence,
  };
  fetch(process.env.REACT_APP_DJANGO_API + 'get_translation/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(getSequence),
  }).then((res) => {
    res.json().then((data) => {
      setTranslation(data);
    });
  });
};

export default getProtein;
