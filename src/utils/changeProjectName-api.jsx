const changeName = (projectId, newName, token) => {
  const nameBody = {
    'order_id': projectId,
    'new_order_name': newName,
  };
  return fetch(process.env.REACT_APP_DJANGO_API + 'update_order_name/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Token ${token}`,
    },
    body: JSON.stringify(nameBody),
  });
};

export default changeName;
