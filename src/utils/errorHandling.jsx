import React, {useState} from 'react';
import {Button, Alert} from 'antd';
import PropTypes from 'prop-types';

const {ErrorBoundary} = Alert;
const ErrorHandler = ({err, status}) => {
  const [error, setError] = useState();
};
ErrorHandler.propTypes = {
  err: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
};
export default ErrorHandler;
