const editProject = (orderId, sequence, features, cookies) => {
  const authToken = `Token ${cookies.token}`;
  const editOrder = {
    'order_id': orderId,
    'sequence': sequence,
    'backbone': {
      'bacterialSelection': 'AmpR',
      'originReplication': 'ColE1',
      'concentration': 40,
      'organism': 'Escherichia coli',
    },
    'features': features,
  };
  fetch(process.env.REACT_APP_DJANGO_API + 'edit_order/', {
    method: 'PATCH',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': authToken,
    },
    body: JSON.stringify(editOrder),
  });
};

export default editProject;
