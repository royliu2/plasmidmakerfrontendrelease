import {message} from 'antd';

const loginApi = (username, password, callbackForToken) => {
  const loginData = {
    'username': username,
    'password': password,
  };
  fetch(process.env.REACT_APP_DJANGO_API + 'api/login/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(loginData),
  }).then((res) => {
    if (res.status !== 200) {
      res.json().then((data) => {
        callbackForToken(data);
      });
    } else {
      res.json().then((data) => {
        callbackForToken(data.token);
      });
    }
  });
};

export default loginApi;
