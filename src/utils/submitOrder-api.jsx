const submitOrder = (projectID, authToken) => {
  const submitOrder = {
    'order_id': projectID,
  };
  fetch(process.env.REACT_APP_DJANGO_API + 'submit_order/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Token ${authToken}`,
    },
    body: JSON.stringify(submitOrder),
  });
};

export default submitOrder;
