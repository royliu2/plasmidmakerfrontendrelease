const getBackbone = (cookies, backboneSearch, setBackboneSelection) => {
  const authToken = `Token ${cookies.token}`;
  const getBackbone = {};
  fetch(process.env.REACT_APP_DJANGO_API + 'search_backbone/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': authToken,
    },
    body: JSON.stringify(getBackbone),
  }).then((res) => {
    if (res.status !== 200) {
      res.json().then(() => {
        return false;
      });
    } else {
      res.json().then((data) => {
        setBackboneSelection(data);
      });
    }
  });
};

export default getBackbone;
