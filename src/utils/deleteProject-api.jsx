const deleteProjectApi = (cookies, id) => {
  const authToken = `Token ${cookies.token}`;
  return fetch(process.env.REACT_APP_DJANGO_API + 'delete_order/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': authToken,
    },
    body: JSON.stringify({
      'order_id': id,
    }),
  });
};

export default deleteProjectApi;
