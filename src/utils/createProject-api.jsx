const createProject = (token) => {
  return fetch(process.env.REACT_APP_DJANGO_API + 'create_order/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Token ${token}`,
    },
  });
};

export default createProject;
