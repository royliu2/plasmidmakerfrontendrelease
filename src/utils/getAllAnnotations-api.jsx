const getAllAnnotations = (token, callBackFunction) => {
  fetch(process.env.REACT_APP_DJANGO_API + 'get_all_annotations/', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'Authorization': `Token ${token}`,
    },
  }).then((res) => {
    if (res.status !== 200) {
      return false;
    } else {
      res.json().then((data) => {
        callBackFunction(data);
      });
    }
  });
};

export default getAllAnnotations;
