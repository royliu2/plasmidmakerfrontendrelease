import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
} from 'react-router-dom';
import {CookiesProvider} from 'react-cookie';
import {Provider} from 'react-redux';

import store from './store';

import Login from './pages/Login';
import Projects from './pages/NoTableProject';
import Home from './pages/Home';
import TestComponent from './pages/TestComponent';
import TestComponent2 from './pages/TestComponent2';

import Register from './pages/Register';
import ErrorHandler from './utils/errorHandling';

const createRoutes = () => {
  return (

    <React.StrictMode>
      <CookiesProvider>
        <Provider store={store}>
          <BrowserRouter>
            <Switch>
              <Route path = "/test" component = {TestComponent}/>
              <Route path = "/test2" component = {TestComponent2}/>
              <Route path = "/projects/:projectId" component={Projects}/>
              <Route path = "/home">
                <Home/>
              </Route>
              <Route path = "/register" component={Register}/>
              <Route path = "/">
                <Login/>
              </Route>
            </Switch>
          </BrowserRouter>
        </Provider>
      </CookiesProvider>
    </React.StrictMode>
  );
};

export default createRoutes;
