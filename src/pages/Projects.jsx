import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {Row, Col, Tabs, Input, Button, Form} from 'antd';
import {useParams, useHistory} from 'react-router-dom';
import {useCookies} from 'react-cookie';
import GlobalHeader from '../components/GlobalHeader';
import verifyToken from '../utils/verifyToken-api';
import getProject from '../utils/getProject-api';
import Visualizer from '../components/visualizer';
import AnnotationList from '../components/AnnotationList';
import getAllAnnotations from '../utils/getAllAnnotations-api';
import AnnotationListSearch from '../components/AnnotationListSearch';
import EditableTable from '../components/PlasmidList';

const {TabPane} = Tabs;
const {TextArea} = Input;
const Projects = () => {
  /**
   * Workflow to prevent race conditions ->
   * projects.jsx <- get search data
   * when user clicks add -> call editorder api
   * call get order to update
   * only get order has the data
   */
  const [cookie, __setCookie] = useCookies(['cookies']);
  const history = useHistory();
  const {projectId} = useParams();
  if (projectId === undefined || projectId === null) {
    history.push('/home');
  }
  const [sequence, setSequence] = useState('');
  const [features, setFeatures] = useState([]);
  const [plasmidName, setPlasmidName] = useState('');
  const [annotationData, setAnnotationData] = useState({});
  const [visualizerName, setVisualizerName] = useState('');
  const [visualizerFeatures, setVisualizerFeatures] = useState({});
  const [visualizerSequence, setVisualizerSequence] = useState('');
  const [lookingAtIndividualPart, setLookingAtIndividualPart] = useState(true);
  useEffect(() => {
    getProject(projectId, setSequence, setFeatures, setPlasmidName,
        setVisualizerName, setVisualizerFeatures, setVisualizerSequence,
        cookie);
    getAllAnnotations(cookie.token, (data) => {
      setAnnotationData(data);
    });
  }, []);
  const addCustomHandler = (newSeq) => {
    const newSequence = newSeq.target.defaultValue;
    const currentLength = sequence.length;
    const newSequenceLength = newSequence.length;
    setSequence(sequence + newSequence);
    const newColor = '#'+(Math.random().toString(16)+'000000').substring(2, 8);
    const newFeature = {
      color: newColor,
      type: 'misc_feature',
      start: currentLength,
      end: currentLength + newSequenceLength,
      id: 'yourUniqueID',
      forward: true,
      idx: features.length + 1,
    };
    const curFeatures = [...features, newFeature];
    setFeatures(curFeatures);
    setVisualizerSequence(sequence + newSequence);
    setVisualizerFeatures(curFeatures);
  };
  return (
    <div>
      <GlobalHeader navspace = {'projects'}/>
      <br/>
      <br/>
      <br/>
      <Row justify="center" align="top" span={12}>
        <Col span = {12}>
          { false === true &&
            <Tabs defaultActiveKey="1">
              <TabPane tab="Search" key="1">
                <div>
                  <AnnotationListSearch
                    annotationData = {annotationData}
                    setAnnotationData = {setAnnotationData}/>
                  <br/>
                  <AnnotationList annotationData = {annotationData}
                    setVisualizerName = {setVisualizerName}
                    setVisualizerFeatures = {setVisualizerFeatures}
                    setVisualizerSequence = {setVisualizerSequence}
                    features = {features}
                  />
                </div>
              </TabPane>
              <TabPane tab="Custom" key="2">
                <TextArea rows={12} onPressEnter={(e) => addCustomHandler(e)}/>
              </TabPane>
            </Tabs>
          }
        </Col>
        <Col span = {12}>
          <EditableTable state = {features}/>
        </Col>
      </Row>
      <Row justify="center" align="top" span={12}>
        <br/>
        <Col span = {24}>
          <Visualizer
            sequence = {visualizerSequence}
            features = {visualizerFeatures}
            name = {visualizerName}
          />
        </Col>
      </Row>
    </div>
  );
};

Projects.propTypes = {
  projectId: PropTypes.string.isRequired,
};

export default Projects;
