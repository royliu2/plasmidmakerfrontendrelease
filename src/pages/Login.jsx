import React, {useEffect} from 'react';
import {useCookies} from 'react-cookie';
import {UserOutlined, LockOutlined} from '@ant-design/icons';
import {Form, Input, Button, message} from 'antd';
import {useHistory} from 'react-router-dom';
import bacteria from '../bacteria.png';
import loginApi from '../utils/login-api';
import verifyToken from '../utils/verifyToken-api';

const Login = () => {
  const [cookies, setCookie] = useCookies(['cookies'], [{sameSite: 'strict'}]);
  const history = useHistory();
  useEffect(() => {
    if (cookies !== null) {
      verifyToken();
    }
  }, []);

  const onLogin = (values) => {
    loginApi(values.username, values.password, (token) => {
      if (typeof token === 'object') {
        message.error(JSON.stringify(token));
      } else {
        setCookie('token', token);
        history.push('/home');
      }
    });
  };

  return (
    <div style={{display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '100vh'}}>
      <Form
        name = "login"
        className = "login-form"
        initialValues = {{remember: true}}
        onFinish = {onLogin}
      >
        <Form.Item style={{'display': 'block',
          'margin-left': 'auto',
          'margin-right': 'auto',
          'width': '50%'}}>
          <img src={bacteria} width="110px" height="110px"/>
        </Form.Item>
        <Form.Item
          name = "username"
          rules = {[
            {
              required: true,
              message: 'Username is required',
            },
          ]}
        >
          <Input
            prefix = {<UserOutlined className="site-form-item-icon"/>}
            placeholder = "username"/>
        </Form.Item>

        <Form.Item
          name = "password"
          rules = {[
            {
              required: true,
              message: 'Password Required',
            },
          ]}
        >
          <Input
            prefix = {<LockOutlined className = "site-form-item-icon"/>}
            type = "password"
            placeholder = "password"
          />
        </Form.Item>

        <Form.Item>
          <Button
            type = "primary"
            htmlType = "submit"
            className = "login-form-button">
          Log In
          </Button>
          <Button onClick={(e) => history.push('/register')}>
            Register
          </Button>
        </Form.Item>

      </Form>

    </div>
  );
};

export default Login;
