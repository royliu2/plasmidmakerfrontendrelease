import React from 'react';
import {Tabs, Table} from 'antd';
import {Layout, Menu, Space, Breadcrumb} from 'antd';
import {UserOutlined, LaptopOutlined} from '@ant-design/icons';

const {SubMenu} = Menu;
const {Header, Content, Footer, Sider} = Layout;
const {TabPane} = Tabs;

const TestComponent2 = () => {
  const columns = [
    {title: 'Batch ID', dataIndex: 'name', key: 'name'},
    {title: 'Progress', dataIndex: 'address', key: 'address'},
    {
      title: 'Action',
      dataIndex: '',
      key: 'x',
      // eslint-disable-next-line react/display-name
      render: () => <a>Edit</a>,
    },
  ];

  const data = [
    {
      key: 1,
      name: 1,
      age: 32,
      address: 'Progress 1',
      description: 'Data will be here',
    },
    {
      key: 2,
      name: 2,
      age: 42,
      address: 'Progress 2',
      description: 'My name is Jim Green, I am 42 years old, living in London No. 1 Lake Park.',
    },
    {
      key: 3,
      name: 3,
      age: 29,
      address: 'N/A',
      description: 'This not expandable',
    },
  ];

  return (
    <Layout>
      <Header className="header">
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
          <Menu.Item key="1">nav 1</Menu.Item>
          <Menu.Item key="2">nav 2</Menu.Item>
          <Menu.Item key="3">nav 3</Menu.Item>
        </Menu>
      </Header>
      <Content style={{padding: '0 50px'}}>
        <Breadcrumb style={{margin: '16px 0'}}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>List</Breadcrumb.Item>
          <Breadcrumb.Item>App</Breadcrumb.Item>
        </Breadcrumb>
        <Layout className="site-layout-background" style={{padding: '24px 0'}}>
          <Sider className="site-layout-background" width={200}>
            <Menu
              mode="inline"
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              style={{height: '100%'}}
            >
              <SubMenu key="sub1" icon={<UserOutlined />} title="Orders">
              </SubMenu>
              <SubMenu key="sub2" icon={<LaptopOutlined />} title="Batches">
              </SubMenu>
            </Menu>
          </Sider>
          <Content style={{padding: '0 24px', minHeight: 280}}>
            <Table
              columns={columns}
              expandable={{
                // eslint-disable-next-line react/display-name
                expandedRowRender: (record) => <p style={{margin: 0}}>{record.description}</p>,
                rowExpandable: (record) => record.name !== 'Not Expandable',
              }}
              dataSource={data}
            />,          </Content>
        </Layout>
      </Content>
    </Layout>
  );
};

export default TestComponent2;
