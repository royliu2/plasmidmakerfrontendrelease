import React from 'react';
import {Tabs, Table} from 'antd';
import {Layout, Menu, Space, Breadcrumb} from 'antd';
import {UserOutlined, LaptopOutlined} from '@ant-design/icons';

const {SubMenu} = Menu;
const {Header, Content, Footer, Sider} = Layout;
const {TabPane} = Tabs;

const TestComponent = () => {
  const columns = [
    {
      title: 'Order ID',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Project Name',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: 'Status',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: 'Batch ID',
      key: 'tags',
      dataIndex: 'tags',
    },
    {
      title: 'Action',
      // eslint-disable-next-line react/display-name
      render: () => <a>Update Batch</a>,
      key: 'action',
    },
  ];

  const data = [
    {
      key: '1',
      name: 12,
      age: 'Super Secret Plasmid',
      address: 'In Progress',
      tags: ['2'],
      action: 'nothing',
    },
    {
      key: '2',
      name: 124,
      age: 'Public Plasmid',
      address: 'In Progress',
      tags: ['2'],
      action: 'test',
    },
    {
      key: '3',
      name: 774,
      age: 'Some custom plasmid',
      address: 'Not Yet Started',
      tags: ['N/A'],
      action: 'test',
    },
  ];
  return (
    <Layout>
      <Header className="header">
        <div className="logo" />
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['2']}>
          <Menu.Item key="1">nav 1</Menu.Item>
          <Menu.Item key="2">nav 2</Menu.Item>
          <Menu.Item key="3">nav 3</Menu.Item>
        </Menu>
      </Header>
      <Content style={{padding: '0 50px'}}>
        <Breadcrumb style={{margin: '16px 0'}}>
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>List</Breadcrumb.Item>
          <Breadcrumb.Item>App</Breadcrumb.Item>
        </Breadcrumb>
        <Layout className="site-layout-background" style={{padding: '24px 0'}}>
          <Sider className="site-layout-background" width={200}>
            <Menu
              mode="inline"
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              style={{height: '100%'}}
            >
              <SubMenu key="sub1" icon={<UserOutlined />} title="Orders">
              </SubMenu>
              <SubMenu key="sub2" icon={<LaptopOutlined />} title="Batches">
              </SubMenu>
            </Menu>
          </Sider>
          <Content style={{padding: '0 24px', minHeight: 280}}>
            <Table columns={columns} dataSource={data} />
          </Content>
        </Layout>
      </Content>
    </Layout>
  );
};

export default TestComponent;
