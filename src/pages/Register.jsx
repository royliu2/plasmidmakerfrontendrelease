import {Form, Input, Button, message} from 'antd';
import {UserOutlined, LockOutlined,
  MailOutlined, HomeOutlined} from '@ant-design/icons';
import React from 'react';
import {
  useHistory,
} from 'react-router-dom';

const Register = () => {
  const history = useHistory();
  const onLogin = (values) => {
    fetch('http://localhost:8000/api/register/', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(values)}).then((res) => {
      if (res.status !== 200) {
        res.json().then( (data) => {
          message.error(`${data.email} \n ${data.username}`);
        },
        );
        if (res.status === 500) {
          history.push('/login');
        }
      } else {
        history.push('/login');
      }
    });
  };
  return (
    <div style={{display: 'flex', justifyContent: 'center',
      alignItems: 'center', height: '100vh'}}>
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
        onFinish={onLogin}
      >
        <Form.Item
          name="username"
          rules={[
            {
              required: true,
              message: 'Please input your Username!',
            },
          ]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Username" />
        </Form.Item>
        <Form.Item
          name="first_name"
          rules={[
            {
              required: true,
              message: 'Please input your First Name!',
            },
          ]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="First Name" />
        </Form.Item>
        <Form.Item
          name="last_name"
          rules={[
            {
              required: true,
              message: 'Please input your Last Name!',
            },
          ]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Last Name" />
        </Form.Item>
        <Form.Item
          name="affiliation"
          rules={[
            {
              required: true,
              message: 'Please input your University',
            },
          ]}
        >
          <Input prefix={<HomeOutlined className="site-form-item-icon" />}
            placeholder="Affiliation" />
        </Form.Item>
        <Form.Item
          name="email"
          rules={[
            {
              type: 'email',
              message: 'The input is not valid E-mail!',
            },
            {
              required: true,
              message: 'Please input your E-mail!',
            },
          ]}
        >
          <Input prefix={<MailOutlined className="site-form-item-icon" />}
            placeholder="E-mail" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: 'Please input your Password!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>

        <Form.Item
          name="confirm_password"
          dependencies={['password']}
          hasFeedback
          rules={[
            {
              required: true,
              message: 'Please confirm your password!',
            },
            ({getFieldValue}) => ({
              validator(_, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }

                return Promise.reject(
                    new Error(
                        'The two passwords that you entered do not match!',
                    ),
                );
              },
            }),
          ]}
        >

          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Confirm Password"
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit"
            className="login-form-button">
            Register
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Register;
