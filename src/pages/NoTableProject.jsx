import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {Row, Col, Tabs, Input, Button, Form, List, Menu, PageHeader,
  Dropdown, Drawer, Divider, Typography, message, Modal}
  from 'antd';
import {DownOutlined, LeftOutlined, RightOutlined, DeleteOutlined, SwapOutlined}
  from '@ant-design/icons';
import {useParams, useHistory} from 'react-router-dom';
import {useCookies} from 'react-cookie';
import GlobalHeader from '../components/GlobalHeader';
import verifyToken from '../utils/verifyToken-api';
import getProject from '../utils/getProject-api';
import Visualizer from '../components/visualizer';
import getBackbone from '../utils/getBackbone-api';
import AnnotationList from '../components/AnnotationList';
import getAllAnnotations from '../utils/getAllAnnotations-api';
import AnnotationListSearch from '../components/AnnotationListSearch';
import submitOrder from '../utils/submitOrder-api';
import changeName from '../utils/changeProjectName-api';
import BackboneList from '../components/Backbone';
import ProjectNameHeader from '../components/ProjectNameHeader';
import editProject from '../utils/editProject-api';
import store from '../store';
import {SimpleCircularOrLinearView} from 'open-vector-editor';
import {SeqViz} from 'seqviz';

const {TabPane} = Tabs;
const {Search} = Input;
const {confirm} = Modal;
const {Title} = Typography;
const Projects = () => {
  /**
   * Workflow to prevent race conditions ->
   * projects.jsx <- get search data
   * when user clicks add -> call editorder api
   * call get order to update
   * only get order has the data
   */
  const [cookie, __setCookie] = useCookies(['cookies']);
  const history = useHistory();
  const {projectId} = useParams();
  if (projectId === undefined || projectId === null) {
    history.push('/home');
  }
  const [sequence, setSequence] = useState('');
  const [features, setFeatures] = useState([]);
  const [plasmidName, setPlasmidName] = useState('');
  const [visualizerDrawer, setVisualizerDrawer] = useState(false);
  const [previewSequence, setPreviewSequence] = useState('');
  const [previewName, setPreviewName] = useState('');
  const [annotationData, setAnnotationData] = useState({});
  const [visualizedAnnotationData, setVisualizedAnnotationData] = useState({});
  const [visualizerName, setVisualizerName] = useState('');
  const [visualizerFeatures, setVisualizerFeatures] = useState({});
  const [visualizerSequence, setVisualizerSequence] = useState('');
  const [backbone, setBackbone] = useState([]);
  const [orderSuccess, setOrderSuccess] = useState(false);
  const [__alignments, __setAlignments] = useState({});
  const [__parts, __setParts] = useState([]);
  const [__plasmids, __setPlasmids] = useState([]);
  useEffect(() => {
    getProject(projectId, setSequence, setFeatures, setPlasmidName,
        setVisualizerName, setVisualizerFeatures, setVisualizerSequence,
        cookie);
    getAllAnnotations(cookie.token, (data) => {
      setAnnotationData(data);
      setVisualizedAnnotationData(data);
    });
  }, []);
  /**
   * iterate over the entire features sequence, anything that is less than
   * the insertion point we ignore
   * everything after we add by length of newseq
   * @param {*} name
   * @param {*} newSeq
   */
  const addCustomHandler = (name, newSeq) => {
    const insertionPos = getNearestInsertionPoint();
    const newSequence = newSeq.toUpperCase();
    const isValidSequence = newSequence.match(/^[ATCG]+$/g) !== null;
    if (!isValidSequence) {
      message.error('Invalid sequence, not exclusively ATCG');
      return;
    }
    const newSequenceLength = newSequence.length;
    const curSequence = `${sequence}`;
    let newFeature = null;
    const newColor = '#'+(Math.random().toString(16)+'000000').substring(2, 8);
    if (insertionPos === 0) {
      newFeature = {
        color: newColor,
        type: 'misc_feature',
        start: insertionPos,
        end: insertionPos + newSequenceLength - 1,
        forward: true,
        name: name,
        custom: true,
      };
    } else if (insertionPos === newSequenceLength + curSequence.length) {
      newFeature = {
        color: newColor,
        type: 'misc_feature',
        start: insertionPos,
        end: insertionPos + newSequenceLength + 1,
        forward: true,
        name: name,
        custom: true,
      };
    } else {
      newFeature = {
        color: newColor,
        type: 'misc_feature',
        start: insertionPos + 1,
        end: insertionPos + newSequenceLength,
        forward: true,
        name: name,
        custom: true,
      };
    }
    const finalSequence = curSequence.slice(
        0, newFeature.start) + newSeq + curSequence.slice(newFeature.start);
    setSequence(finalSequence);
    const curFeatures = [...features];
    curFeatures.sort(featureComparator('start'));
    for (let i = 0; i < curFeatures.length; i++) {
      if (insertionPos <= curFeatures[i].start) {
        curFeatures[i].start += newSequenceLength;
        curFeatures[i].end += newSequenceLength;
      }
    }
    const newFeatures = [...curFeatures, newFeature];
    setFeatures(newFeatures);
    setVisualizerSequence(finalSequence);
    setVisualizerFeatures(newFeatures);
    editProject(projectId, finalSequence,
        newFeatures, cookie);
  };
  const onFinish = (e) => {
    addCustomHandler(e.name, e.sequence);
  };
  const onPreview = () => {
    setVisualizerFeatures(features);
    setVisualizerSequence(sequence);
    setVisualizerName(plasmidName);
  };
  const handleMenuClick = (e) => {
    if (e.key === 'moveLeft') {
      moveLeftDispatch();
    } else if (e.key === 'moveRight') {
      moveRightDispatch();
    } else if (e.key === 'delete') {
      onDelete();
    } else if (e.key == 'direction') {
      changeDirection();
    }
  };
  const invertSequence = (seq) => {
    let newSeq = '';
    const seqInverseTable = {
      'A': 'T',
      'T': 'A',
      'C': 'G',
      'G': 'C',
    };
    for (let i = 0; i < seq.length; i++) {
      newSeq += seqInverseTable[seq[i]];
    }
    return newSeq;
  };
  const changeDirection = () => {
    const featureToChange = getSelectedFeature();
    if (featureToChange === null) {
      message.error('You did not select a fragment, please move the ticker left or right!');
      return;
    }
    const featureDirectionNew = !featureToChange.forward;
    const curFeatures = [...features];
    const idx = curFeatures.findIndex((e) => e.color === featureToChange.color);
    if (idx > -1) {
      curFeatures.splice(idx, 1);
    }
    const newFeature = featureToChange;
    newFeature.forward = featureDirectionNew;
    const newStr = invertSequence(sequence.substring(newFeature.start, newFeature.end + 1));
    const curStr = sequence.substring(0, newFeature.start) + newStr + sequence.substring(newFeature.end + 1);
    setSequence(curStr);
    setVisualizerSequence(curStr);
    const newFeatures = [...curFeatures, featureToChange];
    setVisualizerFeatures(newFeatures);
    setFeatures(newFeatures);
    editProject(projectId, curStr, newFeatures, cookie);
  };
  const moveLeftDispatch = () => {
    const deletedFeature = getSelectedFeature();
    const curFeatures = [...features];
    if (deletedFeature === null) {
      message.error('You did not select a fragment, please move the ticker left or right!');
      return;
    }
    let prevFeature = null;
    curFeatures.sort(featureComparator('start'));
    for (let i = 0; i < curFeatures.length; i++) {
      if (curFeatures[i].color === deletedFeature.color) {
        if (curFeatures.length === 1) {
          prevFeature = curFeatures[0];
        } else if (curFeatures.length === 0) {
          return;
        } else if (curFeatures.length === 2) {
          if (i === 1) {
            prevFeature = curFeatures[0];
          } else {
            prevFeature = curFeatures[1];
          }
        } else {
          if (i === 0) {
            prevFeature = curFeatures[curFeatures.length-1];
          } else {
            prevFeature = curFeatures[i-1];
          }
        }
      }
    }
    const delState = deleteFeature(deletedFeature);

    const finalData = addFeatureAfterFeature(delState, prevFeature);
    setFeatures(finalData.feature);
    setVisualizerSequence(finalData.sequence);
    setVisualizerFeatures(finalData.feature);
    setSequence(finalData.sequence);
    editProject(projectId, finalData.sequence, finalData.feature, cookie);
  };
  const orderSubmissionModal = (projectId) => {
    confirm({
      title: 'Confirm order submission?',
      content: 'This will make the order uneditable',
      okText: 'Yes',
      okType: 'primary',
      cancelText: 'No',
      onOk() {
        submitOrder(projectId, cookie.token);
        message.success({
          content: 'Order submitted',
          className: 'custom-cc',
          style: {
            marginTop: '50vh',
          },
        });
        history.push('/home');
      },
      onCancel() {},
    });
  };
  const moveRightDispatch = () => {
    let prevFeature = null;
    const curFeatures = [...features];
    const deletedFeature = getSelectedFeature();
    if (deletedFeature === null) {
      message.error('You did not select a fragment, please move the ticker left or right!');
      return;
    }
    curFeatures.sort(featureComparator('start'));
    for (let i = 0; i < curFeatures.length; i++) {
      if (curFeatures[i].color === deletedFeature.color) {
        if (curFeatures.length === 1) {
          prevFeature = curFeatures[0];
        } else if (curFeatures.length === 0) {
          return;
        } else if (curFeatures.length === 2) {
          if (i === 1) {
            prevFeature = curFeatures[0];
          } else {
            prevFeature = curFeatures[1];
          }
        } else {
          if (i === curFeatures.length-2) {
            prevFeature = curFeatures[0];
          } else if (i === curFeatures.length - 1) {
            prevFeature = curFeatures[1];
          } else {
            prevFeature = curFeatures[i+2];
          }
        }
      }
    }
    const delState = deleteFeature(deletedFeature);

    const finalData = addFeatureAfterFeature(delState, prevFeature);
    setFeatures(finalData.feature);
    setVisualizerSequence(finalData.sequence);
    setVisualizerFeatures(finalData.feature);
    setSequence(finalData.sequence);
    editProject(projectId, finalData.sequence, finalData.feature, cookie);
  };
  const menu = (
    <Menu onClick={ (e) => handleMenuClick(e)}>
      <Menu.Item key="moveRight" icon={<RightOutlined />}>
        Move Selected Fragment Right
      </Menu.Item>
      <Menu.Item key="moveLeft" icon={<LeftOutlined />}>
        Move Selected Fragment Left
      </Menu.Item>
      <Menu.Item type="danger" key="direction" icon={<SwapOutlined />}>
        Change Direction of Selected Fragment
      </Menu.Item>
      <Menu.Item type="danger" key="delete" icon={<DeleteOutlined />}>
        Delete Selected Fragment
      </Menu.Item>
    </Menu>
  );
  const featureComparator = (featureProperty) => {
    return function(a, b) {
      if (a[featureProperty] > b[featureProperty]) return 1;
      else if (a[featureProperty] < b[featureProperty]) return -1;
      else return 0;
    };
  };
  /**
   * Do the samething as onadd, but subtract by length of the feature seq
   * this will be the delta between the selectedfeature.end and
   *  selectefeature.start
   */
  const onDelete = () => {
    const curFeatures = [...features];
    const selectedFeature = getSelectedFeature();
    if (selectedFeature === null) {
      message.error('You did not select a fragment, please move the ticker left or right!');
      return;
    }
    const idx = curFeatures.findIndex((e) => e.color === selectedFeature.color);
    const deletedFeature = curFeatures[idx];
    if (idx > -1) {
      curFeatures.splice(idx, 1);
    }
    const curSequence = `${sequence}`;
    const newSequence = curSequence.substr(0, deletedFeature.start) +
        curSequence.substr(deletedFeature.end + 1);
    const delta = selectedFeature.end - selectedFeature.start + 1;
    curFeatures.sort(featureComparator('start'));
    for (let i = 0; i < curFeatures.length; i++) {
      if (selectedFeature.start < curFeatures[i].start) {
        curFeatures[i].start -= delta;
        curFeatures[i].end -= delta;
      }
    }
    setSequence(newSequence);
    setVisualizerSequence(newSequence);
    setVisualizerFeatures(curFeatures);
    setFeatures(curFeatures);
    editProject(projectId, newSequence, curFeatures, cookie);
  };
  const deleteFeature = (feature) => {
    const curFeatures = [...features];
    const selectedFeature = feature;
    const deletedState = {
      sequence: null,
      finalFeatures: null,
      removedFeature: null,
      removedSequence: null,
    };
    if (selectedFeature === null) {
      message.error('You did not select a fragment, please move the ticker left or right!');
      return;
    }
    const idx = curFeatures.findIndex((e) => e.color === selectedFeature.color);
    const deletedFeature = curFeatures[idx];
    if (idx > -1) {
      curFeatures.splice(idx, 1);
    }
    const curSequence = `${sequence}`;
    const newSequence = curSequence.substr(0, deletedFeature.start) +
        curSequence.substr(deletedFeature.end + 1);
    const delta = selectedFeature.end - selectedFeature.start + 1;
    const deletedSequence = curSequence.substr(deletedFeature.start, delta);

    curFeatures.sort(featureComparator('start'));
    for (let i = 0; i < curFeatures.length; i++) {
      if (selectedFeature.start < curFeatures[i].start) {
        curFeatures[i].start -= delta;
        curFeatures[i].end -= delta;
      }
    }
    deletedState.sequence = newSequence;
    deletedState.finalFeatures = curFeatures;
    deletedState.removedFeature = feature;
    deletedState.removedSequence = deletedSequence;
    return deletedState;
  };
  const addFeatureAfterFeature = (delState, prevFeature) => {
    let insertionPos = 0;
    for (let i = 0; i < delState.finalFeatures.length; i++) {
      if (delState.finalFeatures[i].color === prevFeature.color) {
        insertionPos = delState.finalFeatures[i].start - 1;
      }
    }
    if (insertionPos === null) {
      message.error('You did not select a fragment, please move the ticker left or right!');
      return;
    }
    const curSequence = `${delState.sequence}`;
    const insertSequence = `${delState.removedSequence}`;
    const newFeature = delState.removedFeature;
    if (insertionPos < 0) {
      insertionPos += 1;
    }
    if (insertionPos === 0) {
      newFeature.start = insertionPos;
      newFeature.end = insertionPos + insertSequence.length - 1;
    } else if (insertionPos === insertSequence.length + curSequence.length) {
      newFeature.start = insertionPos;
      newFeature.end = insertionPos + insertSequence.length + 1;
    } else {
      newFeature.start = insertionPos + 1;
      newFeature.end = insertionPos + insertSequence.length;
    }
    const finalSequence = curSequence.slice(
        0, newFeature.start) + insertSequence + curSequence.slice(newFeature.start);
    const curFeatures = delState.finalFeatures;
    curFeatures.sort(featureComparator('start'));
    for (let i = 0; i < curFeatures.length; i++) {
      if (insertionPos <= curFeatures[i].start) {
        curFeatures[i].start += insertSequence.length;
        curFeatures[i].end += insertSequence.length;
      }
    }
    const newFeatures = [...curFeatures, newFeature];
    const finalData = {
      sequence: finalSequence,
      feature: newFeatures,
    };

    return finalData;
  };
  const getCaretData = () => {
    const caretMetaData = store.getState().VectorEditor.Visualizer;
    if (caretMetaData === undefined || caretMetaData === null) {
      return ( {
        'start': 0,
        'end': null,
      });
    }
    if (caretMetaData.selectionLayer.start !== -1) {
      return ( {
        'start': caretMetaData.selectionLayer.start,
        'end': caretMetaData.selectionLayer.end,
      });
    } else {
      return ({
        'start': caretMetaData.caretPosition,
        'end': null,
      });
    }
  };
  const getNearestInsertionPoint = () => {
    const feature = getSelectedFeature();
    if (feature === null) return 0;
    return feature.end;
  };

  const getSelectedFeature = () => {
    const caretPositions = getCaretData();
    const currentFeatures = [...features];
    currentFeatures.sort(featureComparator('start'));
    let retval = null;
    Object.keys(currentFeatures).forEach((key) => {
      const curFeat = currentFeatures[key];
      if (caretPositions.end !== null) {
        if (curFeat.start <= caretPositions.start && curFeat.end >= caretPositions.end) {
          retval = JSON.parse(JSON.stringify(curFeat));
        }
      } else if (curFeat.start < caretPositions.start && curFeat.end >= caretPositions.start) {
        retval = JSON.parse(JSON.stringify(curFeat));
      }
    });
    return retval;
  };

  const visualizeItem = (item) => {
    setPreviewSequence(item.part_sequence.toLowerCase());
    setVisualizerDrawer(true);
    setPreviewName(item.name);
  };

  const onEdit = (editName) => {
    const currentFeature = getSelectedFeature();
    const allFeatures = [...features];
    const idx = allFeatures.find((x) => x.color === currentFeature.color);
    idx.color = editName;
    setVisualizerFeatures(allFeatures);
  };
  return (
    <div>
      <Drawer
        width={window.innerWidth}
        placement="right"
        closable={true}
        onClose = {() => setVisualizerDrawer(false)}
        visible={visualizerDrawer}
      >
        <Title level={4}>{previewName}</Title>
        <Divider/>
        <Visualizer
          sequence = {previewSequence}
          features = {[]}
          name = {previewName}
          forceUpdate = "true"
        />
      </Drawer>

      <PageHeader
        className="site-page-header"
        onBack={() => history.push('/home')}
        title={'Plasmid Editor' }
        extra = {
          <Button type="primary" style={{'float': 'right'}}
            onClick = { () => orderSubmissionModal(projectId)}>
            Submit Order
          </Button>
        }
      />
      <Divider/>
      <Row justify="center" align="top" span={22} >
        <Col span = {8} height="100%">
          <Row justify="center" align="top" span = {8}>
            <Dropdown overlay={menu} >
              <Button type="primary" style={{width: '30vw'}}>
                   Modify Selected Fragment<DownOutlined />
              </Button>
            </Dropdown>
          </Row>
          <Divider/>
          <Row justify="center" align="top">
            <Tabs defaultActiveKey="1" style={{width: '30vw'}}>
              <TabPane tab="Search" key="1">
                <AnnotationListSearch
                  annotationData = {annotationData}
                  setVisualizedAnnotationData = {setVisualizedAnnotationData}
                  cookie = {cookie.token}/>
                <br/>
                <List header={'Annotations'}
                  bordered
                  dataSource = {visualizedAnnotationData.parts}
                  style = {{overflow: 'auto', height: '50vh'}}
                  renderItem = {(item) => (
                    <div>
                      <List.Item>
                        <List.Item.Meta title = {`Name: ${item.name}`}/>
                        {`id: ${item.id} \n Length: ${item.part_sequence.length}`}
                        <Button
                          style = {{float: 'right'}}
                          onClick = {() =>
                            addCustomHandler(item.name, item.part_sequence)}>
                        Add To Project
                        </Button>
                        <Button style = {{float: 'right'}}
                          onClick = {() => visualizeItem(item)}>
                        View
                        </Button>
                      </List.Item>
                    </div>
                  )}
                />
              </TabPane>
              <TabPane tab="Custom Fragment" key="2">
                <Form onFinish={onFinish}>
                  <Form.Item name={['name']} label="Name">
                    <Input />
                  </Form.Item>
                  <Form.Item name={['sequence']} label="Sequence (5'-3')">
                    <Input.TextArea
                    />
                  </Form.Item>
                  <Form.Item>
                    <Button type="primary" htmlType="submit">
                    Submit
                    </Button>
                  </Form.Item>
                </Form>
              </TabPane>
              <TabPane tab="Rename Selected Fragment" key="3">
                <Search
                  placeholder=''
                  enterButton="Rename"
                  size="large"
                  onSearch={(e) => onEdit(e)}
                />
              </TabPane>
            </Tabs>
          </Row>

        </Col>
        <Col span = {16} >
          <Visualizer
            sequence = {visualizerSequence}
            features = {visualizerFeatures}
            name = {visualizerName}
            forceUpdate="False"
          />
        </Col>
      </Row>
    </div>
  );
};

Projects.propTypes = {
  projectId: PropTypes.string.isRequired,
};

export default Projects;
