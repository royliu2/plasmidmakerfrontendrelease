import React, {useEffect} from 'react';
import verifyToken from '../utils/verifyToken-api';
import ProjectTable from '../components/ProjectTable';
import GlobalHeader from '../components/GlobalHeader';
const Home = () => {
  useEffect(() => {
    verifyToken();
  });

  return (
    <div>
      <GlobalHeader navspace = {`home`}/>
      <ProjectTable/>
    </div>
  );
};

export default Home;
