import React, {useEffect, useState} from 'react';
import {Descriptions, Badge, Collapse, Steps} from 'antd';
import PropTypes from 'prop-types';
import BlobButtonDownloader from '../components/BlobButtonDownloader';

const ProjectDescriptor = ({project, projectName, projectEditDate, quotedPrice, orderStatus, orderId}) => {
  const {Panel} = Collapse;
  const {Step} = Steps;
  const returnSteps = (curStep, error, errormessage, eta) => {
    return (
      <Steps direction="vertical" status = {`${error}`}current={curStep}>
        <Step title="Design Stage" description="You are still designing your plasmid." />
        <Step title="Verification Stage" description="Our automated validation system is verifying your design." />
        {errormessage != null &&
        <Step title="Error" description={`The following violation has been found in your design:
        ${errormessage}`} />
        }
        <Step title="Approval Stage" description="Our team is/will be in contact soon about pricing" />
        <Step title="Synthesis Stage" subTitle={`ETA: ${eta}`}
          description="Your order is currently in the works" />
        <Step title="Shipped" description="Our team is/will be in contact soon about pricing" />
      </Steps>
    );
  };
  const determineBadgeColor = (orderStatus) => {
    if (orderStatus === 'User Construction') {
      return (<Collapse ghost={true}>
        <Panel header={<Badge status={'processing'}text = {orderStatus}/>} key="1">
          {returnSteps(0, '', null, 'xx/xx/xx')}
        </Panel>
      </Collapse>
      );
    } else if (orderStatus === 'Quote Request') {
      return (<Collapse ghost={true}>
        <Panel header={<Badge status={'processing'}text = {orderStatus}/>} key="1">
          {returnSteps(1, '', null, 'xx/xx/xx')}
        </Panel>
      </Collapse>
      );
    } else {
      return (<Collapse ghost={true}>
        <Panel header={<Badge status={'error'}text = "An error has been detected" />} key="1">
          {returnSteps(2, 'error', orderStatus, 'xx/xx/xx')}
        </Panel>
      </Collapse>
      );
    }
  };
  return (
    <Descriptions title = {projectName} bordered>
      <Descriptions.Item label="Order Status" span={12}>
        {determineBadgeColor(orderStatus)}
      </Descriptions.Item>
      <Descriptions.Item label="Assembly Data" span={12}>
        <BlobButtonDownloader
          blobData = {project.file_contents ? project.file_contents : project.has_file_contents}
          hasBlobString = {`${orderId} Assembly Data`}
          noBlobString = {`Project in progress`}
          zipName = {`${project.order_id}.zip`}
        /></Descriptions.Item>
      <Descriptions.Item label="Order ID" span={15}>{orderId}</Descriptions.Item>
      <Descriptions.Item label="Quoted Price">{quotedPrice}</Descriptions.Item>
      <Descriptions.Item label="Last Edited">{Date(parseInt(projectEditDate)*1000).toString()}</Descriptions.Item>
    </Descriptions>
  );
};


ProjectDescriptor.propTypes = {
  project: PropTypes.any.isRequired,
  projectName: PropTypes.string.isRequired,
  projectEditDate: PropTypes.string.isRequired,
  quotedPrice: PropTypes.string.isRequired,
  orderStatus: PropTypes.string.isRequired,
  orderId: PropTypes.number.isRequired,
};
export default ProjectDescriptor;
