import React, {useEffect, useState} from 'react';
import {Collapse, Button, Modal, List, Row, Col, Form, Input} from 'antd';
import createProject from '../utils/createProject-api';
import {useCookies} from 'react-cookie';
import getProjectMetaData from '../utils/getProjectMetadata-api';
import deleteProjectApi from '../utils/deleteProject-api';
import changeName from '../utils/changeProjectName-api';
import {useHistory} from 'react-router';
import ProjectDescriptor from '../components/ProjectDescriptor';
const ProjectTable = () => {
  const [projects, setProjects] = useState([]);
  const history = useHistory();
  const {confirm} = Modal;
  const {Panel} = Collapse;
  const [cookies, setCookies] = useCookies(['cookies']);
  const viewProject = (projectId) => {
    history.push(`/projects/${projectId}`);
  };
  useEffect(() => {
    getProjectMetaData(cookies, setCookies, projects, setProjects);
  }, []);
  const deleteProject = (id) => {
    const title = `Are you sure you want to delete project ${id}?`;
    confirm({
      title: title,
      content: 'This action is irreversible',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        deleteProjectApi(cookies, id).then(() => {
          getProjectMetaData(cookies, setCookies, projects, setProjects);
        });
      },
      onCancel() {},
    });
  };
  const renameProject = (id, name) => {
    const title = `Rename Your Project`;
    confirm({
      title: title,
      content: <Input id="RenameInput" style={{width: '30%'}} defaultValue={`${name}`} />,
      okText: 'Yes',
      okType: 'primary',
      cancelText: 'No',
      onOk() {
        changeName(id, RenameInput.value, cookies.token).then(() => {
          getProjectMetaData(cookies, setCookies, projects, setProjects);
        });
      },
      onCancel() {},
    });
  };
  const FormData = () => {
    return (
      <Form
        name="basic"
        labelCol={{span: 8}}
        wrapperCol={{span: 16}}
        initialValues={{remember: true}}
        onFinish={onFinish}
        // onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[{message: 'Please input your username!'}]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{required: true, message: 'Please input your password!'}]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item wrapperCol={{offset: 8, span: 16}}>
          <Button type="primary" htmlType="submit">
          Submit
          </Button>
        </Form.Item>
      </Form>
    );
  };
  const newProject = (cookies) => {
    createProject(cookies.token).then(() => {
      getProjectMetaData(cookies, setCookies, projects, setProjects);
    });
  };
  return (
    <div>
      <br/>
      <Button style = {{float: 'right'}} type = 'primary'
        onClick = {() => newProject(cookies)}>
        Add Project</Button>
      <br/>
      <br/>
      <br/>
      <br/>

      <Row>
        <Col span={24}>
          <List
            header={'Projects'}
            bordered
            itemLayout = 'horizontal'
            dataSource = {projects}
            renderItem = {(item) => (
              <div>
                <Button style = {{float: 'right'}}
                  type = 'link' onClick = {() => viewProject(item.order_id)}>
                  {`Edit project ${item.order_id}`}
                </Button>
                <Button style = {{float: 'right'}}
                  type = 'danger' onClick = {() => deleteProject(item.order_id)}>
                  {`Delete project ${item.order_id}`}
                </Button>
                <Button style= {{float: 'right'}}
                  type='link' onClick = {() => renameProject(item.order_id, item.order_name)}>
                  {`Rename Project ${item.order_id}`}
                </Button>
                <List.Item>
                  <ProjectDescriptor project={item} projectName={item.order_name} projectEditDate={item.updated_at}
                    quotedPrice = '$N/A' orderStatus = {item.status} orderId = {item.order_id}/>
                </List.Item>
              </div>
            )}
          />
        </Col>
      </Row>

    </div>
  );
};

export default ProjectTable;
