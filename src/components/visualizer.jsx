import React, {useEffect, useState} from 'react';
import {Editor, updateEditor} from 'open-vector-editor';
import {Switch} from 'antd';
import PropTypes from 'prop-types';
import store from '../store';

const Visualizer = ({sequence, features, name, forceUpdate}) => {
  const [aaView, setaaView] = useState(false);
  const [aaData, setaaData] = useState(forceUpdate);
  const [vizSeq, setVizSeq] = useState(sequence);
  const size = useWindowSize();

  React.useEffect(() => {
    setVizSeq(sequence);
    updateEditor(store, 'Visualizer', {
      readOnly: true,
      caretPosition: 0,
      primers: false,
      parts: false,
      orTranslations: false,
      sequenceData: {
        isProtein: aaView,
        name: name,
        circular: true,
        proteinSequence: aaData,
        sequence: vizSeq,
        features: features,
      },
      panelsShown: [
        [{
          id: 'sequence',
          name: 'Sequence Map',
          active: true,
        },
        {
          id: 'circular',
          name: 'Circular Map',
          active: true,
        },
        {
          id: 'rail',
          name: 'Linear Map',
          active: false,
        },
        ]],
    });
  });
  const editorProps = {
    editorName: 'Visualizer',
    isFullScreen: false,
    showMenuBar: false,
    height: size.height - 180,
    showReadOnly: true,
    ToolBarProps: {
      toolList: ['downloadTool'],
    },

  };
  return (
    <div>
      <Editor {...editorProps}/>
    </div>
  );
};

const useWindowSize = () => {
  const [windowSize, setWindowSize] = useState(
      {width: undefined, height: undefined,
      });
  useEffect(() => {
    /**
     * jsdoc
     */
    function handleResize() {
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }
    window.addEventListener('resize', handleResize);
    handleResize();
    return () => window.removeEventListener('resize', handleResize);
  }, []);
  return windowSize;
};
Visualizer.propTypes = {
  sequence: PropTypes.string.isRequired,
  features: PropTypes.array.isRequired,
  name: PropTypes.string.isRequired,
  forceUpdate: PropTypes.string,
};

export default Visualizer;

