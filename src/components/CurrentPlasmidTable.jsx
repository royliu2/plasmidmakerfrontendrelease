import React, {useContext, useState, useEffect, useRef} from 'react';
import PropTypes from 'prop-types';
import {Table, Input, Button, Popconfirm, Form} from 'antd';
import {MenuOutlined} from '@ant-design/icons';
import {sortableContainer,
  sortableElement, sortableHandle} from 'react-sortable-hoc';
import arrayMove from 'array-move';
const EditableContext = React.createContext(null);
const DragHandle = sortableHandle(() =>
  <MenuOutlined style={{cursor: 'grab', color: '#999'}} />);
const SortableItem = sortableElement((props) => <tr {...props} />);
const SortableContainer = sortableContainer((props) => <tbody {...props} />);

const EditableRow = ({__index, ...props}) => {
  const [form] = Form.useForm();
  return (
    <Form form = {form} component = {false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

const CurrentPlasmidTable = ({data}) => {
  const [plasmidsList, setPlasmidsList] = useState(data);

  onSortEnd = ({oldIndex, newIndex}) => {
    if (oldIndex !== newIndex) {
      const newData = arrayMove([].concat(plasmidsList), oldIndex, newIndex)
          .filter((el) => !!el);
      setPlasmidsList({data: newData});
    }
  };

  // DraggableContainer = (props) => (
  //   <SortableContainer
  //     useDragHandle
  //     disableAutoscroll
  //     helperClass="row-dragging"
  //     onSortEnd={onSortEnd}
  //     {...props}
  //   />
  // );
};
EditableRow.propTypes = {
  __index: PropTypes.number,
};
SortableContainer.displayName = 'Sortablecontainer';
export default CurrentPlasmidTable;
