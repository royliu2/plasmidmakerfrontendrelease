import React from 'react';
import {List, Button} from 'antd';
import PropTypes from 'prop-types';
const AnnotationList = (annotationData) => {
  const styleType = {
    overflow: 'auto',
    height: '300px',
  };
  const visualizeItem = (item) => {
    annotationData.setVisualizerName(item.name);
    annotationData.setVisualizerSequence(item.part_sequence);
    annotationData.setVisualizerFeatures({});
  };
  const addToProject = (item) => {
    annotationData.addCustomHandler(item.name, item.part_sequence);
  };

  return (
    <List header={'Annotations'}
      bordered
      dataSource = {annotationData.annotationData.parts}
      style = {styleType}
      renderItem = {(item) => (
        <div>
          <List.Item>
            <List.Item.Meta title = {`Name: ${item.name}`}/>
            {`id: ${item.id} \n Length: ${item.part_sequence.length}`}
            <Button
              style = {{float: 'right'}}
              onClick = {() => addToProject(item)}>
              Add To Project
            </Button>

            <Button style = {{float: 'right'}}
              onClick = {() => visualizeItem(item)}>
              View</Button>
          </List.Item>
        </div>
      )}>
    </List>
  );
};

AnnotationList.propTypes = {
  annotationData: PropTypes.array.isRequired,
};

export default AnnotationList;
