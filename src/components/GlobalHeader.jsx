import React from 'react';
import {Menu} from 'antd';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';
import {useCookies} from 'react-cookie';
const GlobalHeader = ({navspace}) => {
  const history = useHistory();
  const [cookies, setCookies] = useCookies(['cookies']);
  const moveSite = (key) => {
    history.push(`/${key}`);
  };
  return (
    <div className="logo">
      <Menu style={{width: '100vw', float: 'right'}}
        mode="horizontal" defaultSelectedKeys={[navspace]}
        onClick={(item) => moveSite(item.key)}>
        <Menu.Item key="home">Projects</Menu.Item>
      </Menu>
    </div>
  );
};

GlobalHeader.propTypes = {
  navspace: PropTypes.string.isRequired,
};

export default GlobalHeader;
