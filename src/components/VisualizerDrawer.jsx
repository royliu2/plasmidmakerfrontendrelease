import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Drawer, List, Avatar, Divider, Col, Row} from 'antd';
const VisualizerDrawer = ({sequence, name, showDrawer}) => {
  if (showDrawer) {
    return (
      <Drawer
        width={640}
        placement="right"
        closable={false}
        visible={showDrawer}
      ></Drawer>
    );
  };
};

VisualizerDrawer.propTypes = {
  sequence: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  showDrawer: PropTypes.bool.isRequired,
};
export default VisualizerDrawer;
