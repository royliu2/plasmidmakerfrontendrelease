import React, {useState, useEffect} from 'react';
import {Input} from 'antd';
import PropTypes from 'prop-types';

const ProjectNameHeader = ({projectName}) => {
  const [curProjectName, setCurProjectName] = useState(projectName);
  useEffect(() => {
    console.log(projectName);
  }, []);
  const val = <Input size="large" bordered={false}
    defaultValue={`${projectName}`}/>;
  return (
    val
  );
};

ProjectNameHeader.propTypes = {
  projectName: PropTypes.string.isRequired,
};

export default ProjectNameHeader;
