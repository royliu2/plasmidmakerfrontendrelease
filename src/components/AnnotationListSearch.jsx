import React, {useState} from 'react';
import {Input, Checkbox, Radio, Switch} from 'antd';
import Fuse from 'fuse.js';
const {Search} = Input;


const AnnotationListSearch = (annotationList) => {
  const [searchQuery, setSearchQuery] = useState('Name');
  const [exactSearch, setExactSearch] = useState(false);
  const onSearch = (value) => {
    // getAllAnnotations(annotationList.cookie, (data) => {
    //   annotationList.setAnnotationData(data);
    // });
    if (value.length === 0) {
      annotationList.setVisualizedAnnotationData(
          {'parts': annotationList.annotationData.parts});
      return;
    }
    let options = {
      keys: ['name', 'part_sequence'],
    };
    if (searchQuery === 'Name') {
      options = {
        keys: ['name'],
      };
    } else {
      options = {
        keys: ['part_sequence'],
      };
    }
    if (exactSearch) {
      options['threshold'] = 0.0;
    } else {
      options['threshold'] = 0.6;
    }
    const fuse = new Fuse(annotationList.annotationData.parts, options);
    const searchRes = fuse.search(value);
    const finalSearch = searchRes.map( (obj) => {
      return obj.item;
    });
    annotationList.setVisualizedAnnotationData({'parts': finalSearch});
  };
  const onChange = (e) => {
    setSearchQuery(e.target.value);
  };
  const exactSearchChange = (e) => {
    setExactSearch(e);
  };
  return (
    <div>
      <Search placeholder="" allowClear onSearch={onSearch}
        style={{width: '30vw'}} />
      <br/>
      <Switch checkedChildren="Exact Matches" unCheckedChildren="Close Matches"
        onChange={exactSearchChange}></Switch>

      <br/>
      <br/>
      <Radio.Group onChange={onChange} value={searchQuery}>
        <Radio value={'Name'}>Search By Name</Radio>
        <Radio value={'Sequence'}>Search By Sequence</Radio>

      </Radio.Group>
    </div>
  );
};

export default AnnotationListSearch;
