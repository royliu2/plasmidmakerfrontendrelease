import React from 'react';
import {Button} from 'antd';
import {DownloadOutlined} from '@ant-design/icons';

import PropTypes from 'prop-types';

const BlobButtonDownloader = ({blobData, hasBlobString, noBlobString, zipName}) => {
  const cleanBlobData = (blob) => {
    blob = blob.substring(2, blob.length-1);
    blob = blob.replace(/\\n/g, '');
    const rawBytes = atob(blob);
    let base64 = '';
    for (let i = 0; i < rawBytes.length; i++) {
      base64 += String.fromCharCode(rawBytes.charCodeAt(i) & 0xFF);
    }
    const zipBytes = new Uint8Array(base64.length);
    for (let i = 0; i < base64.length; i++) {
      zipBytes[i] = base64.charCodeAt(i);
    }
    const zipFile = new Blob([zipBytes], {type: 'application/zip'});
    saveAs(zipFile, zipName);
  };
  if (blobData === false) {
    return (
      <Button type="primary" disabled icon={<DownloadOutlined />}>
        {noBlobString}
      </Button>
    );
  }
  return (
    <Button type="primary"
      onClick={() => cleanBlobData(blobData)}
      icon={<DownloadOutlined />}>
      {hasBlobString}
    </Button>
  );
};

BlobButtonDownloader.propTypes = {
  blobData: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
  hasBlobString: PropTypes.string.isRequired,
  noBlobString: PropTypes.string.isRequired,
  zipName: PropTypes.string.isRequired,
};

export default BlobButtonDownloader;
