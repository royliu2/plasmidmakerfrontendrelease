import React, {useState, useEffect} from 'react';
import {TreeSelect} from 'antd';
import getBackbone from '../utils/getBackbone-api';
import PropTypes from 'prop-types';

const options = [{value: 'gold'}, {value: 'lime'},
  {value: 'green'}, {value: 'cyan'}];

const {TreeNode} = TreeSelect;
const BackboneList = ({cookies, setBackbone}) => {
  const [searchQuery, setSearchQuery] = useState([]);
  const [backboneSelection, setBackboneSelection] = useState(null);
  const [curBackbone, setCurBackbone] = useState([]);
  useEffect(() => {
    getBackbone(cookies, searchQuery, setBackboneSelection);
  }, []);
  const onChange = (data) => {
    setBackbone(data);
  };
  const renderTreeNodes = (data) =>{
    if (data === null) return;
    data.plasmids.map((item) => {
      console.log(item);
      if (item) {
        return (
          <TreeNode title={item.name} key={item.name} dataRef={item}>
            {/* {renderTreeNodes(item.children)} */}
          </TreeNode>
        );
      }
      return <TreeNode key={item.key} {...item} />;
    });
  };
  return (
    <div>
      <TreeSelect
        showSearch
        value={curBackbone}
        style={{width: '100%'}}
        dropdownStyle={{maxHeight: 400, overflow: 'auto'}}
        placeholder='Select Thing'
        allowClear
        multiple
        treeDefaultExpandAll
        onChange={((data) => onChange(data))}
      >
        {renderTreeNodes(backboneSelection)}
      </TreeSelect>
    </div>
  );
};

BackboneList.propTypes = {
  cookies: PropTypes.string,
  setBackbone: PropTypes.func,
};

export default BackboneList;
