# PlasmidMaker Frontend
## Setup
To run the code locally, please first ensure that the django server is running. If it is not running at `127.0.0.1:8000`, please change the `.env` file to correct to the ip address that your backend server is running at. Then run:
```
yarn install
yarn start
```
Check `localhost:3000` or `127.0.0.1:3000` for the website, you should be greeted with a login page. Corresponding pings to the backend upon user registration and attempted login should show up assuming debug logs are running on the backend. If there are issues, you should attempt to debug through your local browswer console. 

## License
Code is under MIT license and provided as-is with no warranties of any kind. Please refer to license.txt for more details.